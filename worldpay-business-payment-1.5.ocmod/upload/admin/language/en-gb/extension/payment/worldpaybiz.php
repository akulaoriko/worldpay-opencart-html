<?php
// Heading
$_['heading_title']					= 'Worldpay Business Gateway';

// Text
$_['text_extension']				= 'Extensions';
$_['text_payment']					= 'Payment';
$_['text_success']					= 'Success: You have modified WorldPay account details!';
$_['text_edit']                     = 'Edit WorldPay';
$_['text_worldpaybiz']					= '<a href="https://business.worldpay.com/partner/opencart" target="_blank"><img src="view/image/payment/worldpay.png" alt="Worldpay" title="Worldpay" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_successful']				= 'On - Always Successful';
$_['text_declined']					= 'On - Always Declined';
$_['text_off']						= 'Off';

// Entry
$_['entry_merchant']				= 'Merchant ID';
$_['entry_password']				= 'Payment Response Password';
$_['entry_md5_secret']				= 'Md5 Secret';
$_['entry_signature']				= 'Signature';
$_['entry_callback']				= 'Relay Response URL';
$_['entry_test']					= 'Test Mode';
$_['entry_total']					= 'Total';
$_['entry_order_status']			= 'Order Status';
$_['entry_geo_zone']				= 'Geo Zone';
$_['entry_status']					= 'Status';
$_['entry_sort_order']				= 'Sort Order';

// Help
$_['help_password']					= 'This has to be set in the WorldPay control panel.';
$_['help_callback']					= 'This has to be set in the WorldPay control panel. You will also need to check the "Enable the Shopper Response".';
$_['help_total']					= 'The checkout total the order must reach before this payment method becomes active.';
$_['help_md5_secret']				= 'This has to be set in the WorldPay control panel.';
$_['help_signature']				= 'This has to be set in the WorldPay control panel.';

// Error
$_['error_permission']				= 'Warning: You do not have permission to modify payment WorldPay!';
$_['error_merchant']				= 'Merchant ID Required!';
$_['error_password']				= 'Password Required!';