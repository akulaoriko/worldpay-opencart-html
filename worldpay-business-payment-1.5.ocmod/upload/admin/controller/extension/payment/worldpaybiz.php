<?php
class ControllerExtensionPaymentWorldPaybiz extends Controller {
	private $error = array();

	public function index() {
    
		$this->load->language('extension/payment/worldpaybiz');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('payment_worldpaybiz', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true));
		}

		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_successful'] = $this->language->get('text_successful');
		$data['text_declined'] = $this->language->get('text_declined');
		$data['text_off'] = $this->language->get('text_off');

		$data['entry_merchant'] = $this->language->get('entry_merchant');
		$data['entry_password'] = $this->language->get('entry_password');
    $data['entry_md5_secret'] = $this->language->get('entry_md5_secret');
		$data['entry_callback'] = $this->language->get('entry_callback');
		$data['entry_test'] = $this->language->get('entry_test');
		$data['entry_total'] = $this->language->get('entry_total');
		$data['entry_order_status'] = $this->language->get('entry_order_status');
		$data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');

		$data['help_password'] = $this->language->get('help_password');
		$data['help_callback'] = $this->language->get('help_callback');
		$data['help_total'] = $this->language->get('help_total');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['merchant'])) {
			$data['error_merchant'] = $this->error['merchant'];
		} else {
			$data['error_merchant'] = '';
		}

		if (isset($this->error['password'])) {
			$data['error_password'] = $this->error['password'];
		} else {
			$data['error_password'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/payment/worldpaybiz', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/payment/worldpaybiz', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true);

		if (isset($this->request->post['payment_worldpaybiz_merchant'])) {
			$data['payment_worldpaybiz_merchant'] = $this->request->post['payment_worldpaybiz_merchant'];
		} else {
			$data['payment_worldpaybiz_merchant'] = $this->config->get('payment_worldpaybiz_merchant');
		}

		if (isset($this->request->post['payment_worldpaybiz_password'])) {
			$data['payment_worldpaybiz_password'] = $this->request->post['payment_worldpaybiz_password'];
		} else {
			$data['payment_worldpaybiz_password'] = $this->config->get('payment_worldpaybiz_password');
		}
    
    if (isset($this->request->post['payment_worldpaybiz_md5_secret'])) {
			$data['payment_worldpaybiz_md5_secret'] = $this->request->post['payment_worldpaybiz_md5_secret'];
		} else {
			$data['payment_worldpaybiz_md5_secret'] = $this->config->get('payment_worldpaybiz_md5_secret');
		}
    
    if (isset($this->request->post['payment_worldpaybiz_signature'])) {
			$data['payment_worldpaybiz_signature'] = $this->request->post['payment_worldpaybiz_signature'];
		} else {
			$data['payment_worldpaybiz_signature'] = $this->config->get('payment_worldpaybiz_signature');
		}
    
    if (isset($this->request->post['payment_worldpaybiz_relay_response_url'])) {
			$data['payment_worldpaybiz_relay_response_url'] = $this->request->post['payment_worldpaybiz_relay_response_url'];
		} else {
			$data['payment_worldpaybiz_relay_response_url'] = $this->config->get('payment_worldpaybiz_relay_response_url');
		}
    
		$data['callback'] = HTTPS_CATALOG . 'index.php?route=extension/payment/worldpaybiz/callback';

		if (isset($this->request->post['payment_worldpaybiz_test'])) {
			$data['payment_worldpaybiz_test'] = $this->request->post['payment_worldpaybiz_test'];
		} else {
			$data['payment_worldpaybiz_test'] = $this->config->get('payment_worldpaybiz_test');
		}

		if (isset($this->request->post['payment_worldpaybiz_total'])) {
			$data['payment_worldpaybiz_total'] = $this->request->post['payment_worldpaybiz_total'];
		} else {
			$data['payment_worldpaybiz_total'] = $this->config->get('payment_worldpaybiz_total');
		}

		if (isset($this->request->post['payment_worldpaybiz_order_status_id'])) {
			$data['payment_worldpaybiz_order_status_id'] = $this->request->post['payment_worldpaybiz_order_status_id'];
		} else {
			$data['payment_worldpaybiz_order_status_id'] = $this->config->get('payment_worldpaybiz_order_status_id');
		}

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		if (isset($this->request->post['payment_worldpaybiz_geo_zone_id'])) {
			$data['payment_worldpaybiz_geo_zone_id'] = $this->request->post['payment_worldpaybiz_geo_zone_id'];
		} else {
			$data['payment_worldpaybiz_geo_zone_id'] = $this->config->get('payment_worldpaybiz_geo_zone_id');
		}

		$this->load->model('localisation/geo_zone');

		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		if (isset($this->request->post['payment_worldpaybiz_status'])) {
			$data['payment_worldpaybiz_status'] = $this->request->post['payment_worldpaybiz_status'];
		} else {
			$data['payment_worldpaybiz_status'] = $this->config->get('payment_worldpaybiz_status');
		}

		if (isset($this->request->post['payment_worldpaybiz_sort_order'])) {
			$data['payment_worldpaybiz_sort_order'] = $this->request->post['payment_worldpaybiz_sort_order'];
		} else {
			$data['payment_worldpaybiz_sort_order'] = $this->config->get('payment_worldpaybiz_sort_order');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/payment/worldpaybiz', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/payment/worldpaybiz')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->request->post['payment_worldpaybiz_merchant']) {
			$this->error['merchant'] = $this->language->get('error_merchant');
		}

		if (!$this->request->post['payment_worldpaybiz_password']) {
			$this->error['password'] = $this->language->get('error_password');
		}

		return !$this->error;
	}
}